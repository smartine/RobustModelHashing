package bandLSH;


public class ModelRepositoryItem {
	String modelName;
	int [] featureVector;
	int [] bandVector;
	
	public ModelRepositoryItem(String modelName, int[] featureVector, int [] bandVector) {
		super();
		this.modelName = modelName;
		this.featureVector = featureVector;
		this.bandVector = bandVector;
	}
}
