package bandLSH;

public class BucketArray {
	int numBuckets;
	BucketItem [] bucketArray;
	
	
	public BucketArray(int numBuckets) {
		super();
		this.numBuckets = numBuckets;
		bucketArray = new BucketItem[numBuckets];
	}
	
}
