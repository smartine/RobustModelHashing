package bandLSH;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.cea.lise.security.hashing.HashComparator;

public class BandLSH {
	
	int numBuckets;

	int numbands;
	//This are our models
	HashMap<String, int []> modelHashMap = new HashMap<String, int[]>();
	HashMap<Integer, Integer[]> buckets = new HashMap<Integer, Integer[]>(); 

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//int[] firstBand = Arrays.copyOfRange(signature, 0, bandSize);
		//int position = Arrays.hashCode(firstBand) % numBuckets;
		//System.out.println("firstBand= " + Arrays.hashCode(firstBand));
		//band1[position] = signature;
		//Integer[] s = Arrays.stream(signature).boxed().toArray( Integer[]::new );
		//bands.put(position, s);

	}
	
	public BandLSH(int numBuckets, int numbands, HashMap<String, int[]> modelHashMap) {
		super();
		this.numBuckets = numBuckets;
		this.numbands = numbands;
		this.modelHashMap = modelHashMap;
	}
	
	public HashMap<Set<String>, Collision> doBands() {
		int vectorSize = (modelHashMap.values().iterator().next().length);
		int bandSize = vectorSize / numbands;
		
		//We store collisions
		HashMap<Set<String>, Collision> colHashMap = new HashMap<Set<String>, Collision>();
		
		//We need a bucketList for each band
		BucketArray [] BucketListsArray = new BucketArray[numbands];
		for (int i = 0; i < numbands; i++) {
			BucketListsArray[i] = new BucketArray(numBuckets);
		}
		
		int numCollision = 0;
		
		Iterator it = modelHashMap.entrySet().iterator();
		//System.out.println("\nDoingBands\n");
		while (it.hasNext()) {
			Map.Entry pair = (Map.Entry)it.next();
			int [] sourceVector = (int[]) pair.getValue();
			String sourceName = (String) pair.getKey();
			//System.out.println(sourceName);
			for (int i = 0; i < numbands; i++) {
				int[] band = Arrays.copyOfRange(sourceVector, i*bandSize, ((i+1)*bandSize));
				int position = Math.floorMod(Arrays.hashCode(band), numBuckets);
				BucketArray hashList = BucketListsArray[i];
				if (hashList.bucketArray[position] == null) {
					//the bucket is empty
					ModelRepositoryItem mri = new ModelRepositoryItem(sourceName, sourceVector, band);
					List<ModelRepositoryItem> itemList = new ArrayList();
					itemList.add(mri);
					BucketItem item = new BucketItem(itemList);
					hashList.bucketArray[position] = item;
				}else {
					//this is a collision
					ModelRepositoryItem mri = new ModelRepositoryItem(sourceName, sourceVector, band);
					BucketItem item = hashList.bucketArray[position];
					item.itemList.add(mri);
					
					for (String targetName : item.getMemberNames()) {
						Set<String> currentPair = Stream.of(sourceName, targetName).collect(Collectors.toSet());
						if (currentPair.size() == 2) {
							numCollision++;
							if (colHashMap.containsKey(currentPair)) {
								Collision c = colHashMap.get(currentPair);
								c.addBand(i);
							}else {
								Collision c = new Collision(sourceName, targetName, i);
								colHashMap.put(currentPair, c);
							}
						
						}
					}
					//System.out.println("COLLISION between " + Arrays.toString(item.getMemberNames().toArray()) + " at band " + i);
				}
			}
		}
		
		//System.out.println("Num collisions = " + numCollision);
		//System.out.println("Pair collisions = " + colHashMap.size());
		return colHashMap;
	}
	
	/**
	 * Calculates average similarity of collision pairs. There is sure a more elegant and generic solution
	 * using delegation and maybe lambdas...
	 * @param colHashMap
	 * @param modelHashMap
	 * @return
	 */
	public double calculateAverageSim(HashMap<Set<String>, Collision> colHashMap, HashMap<String, int []> modelHashMap) {
		double sum = 0.0;
		Collection<Collision> collisions = colHashMap.values();
		for (Collision c : collisions) {
			Object [] models = c.getModels().toArray();
			sum += HashComparator.hammingSim(modelHashMap.get(models[0]), modelHashMap.get(models[1]));
		}
		return sum == 0.0 ? 0.0 : sum/collisions.size();
	}
	
	public void printPairs(HashMap<Set<String>, Collision> colHashMap, int threshold, boolean filter) {
		Collection<Collision> collisions = colHashMap.values();
		for (Collision c : collisions) {
			Object [] models = c.getModels().toArray();
			double sim = HashComparator.hammingSim(modelHashMap.get(models[0]), modelHashMap.get(models[1]));
			if (sim > threshold || !filter) {
			System.out.println("Collision: " + models[0] + " and " + models[1] + " bands: " + c.getBands().size()
					+ " sim: " + sim);
			}
		}
	}

}
