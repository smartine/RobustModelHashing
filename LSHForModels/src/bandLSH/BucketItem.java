package bandLSH;

import java.util.ArrayList;
import java.util.List;

public class BucketItem {
	List<ModelRepositoryItem> itemList;
	List<String> memberNames;

	public BucketItem(List<ModelRepositoryItem> itemList) {
		super();
		this.itemList = itemList;
	}
	
	public List<String> getMemberNames() {
		List<String>  names = new ArrayList();
		for (ModelRepositoryItem item : itemList) {
		    names.add(item.modelName);
		}
		return names;
	}

}
