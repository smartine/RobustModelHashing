package bandLSH;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Collision {
	public List<Integer> getBands() {
		return bands;
	}

	public void setBands(List<Integer> bands) {
		this.bands = bands;
	}

	Set<String> models;
	List<Integer>  bands;
	
	
	public Collision(String sourceName, String targetName, int band) {
		super();
		models = new HashSet<String>();
		models.add(sourceName);
		models.add(targetName);
		bands = new ArrayList<Integer>();
		this.addBand(band);
	}
	
	public void addBand (int i) {
		this.bands.add(i);
	}

	public Set<String> getModels() {
		return models;
	}

	public void setModels(Set<String> models) {
		this.models = models;
	}

}
