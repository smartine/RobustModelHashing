package tests;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.cea.lise.security.hashing.EcoreMetamodelManager;
import org.cea.lise.security.hashing.HashComparator;
import org.cea.lise.security.hashing.ModelHashing;
import org.cea.lise.security.hashing.ModelUtil;
import org.eclipse.emf.ecore.EObject;

import bandLSH.BandLSH;

public class TestLSHForMetamodels {

	public static void main(String[] args) {
		System.out.println("Test Test");
		
		//We store here the masks obtained for each metamodel
		HashMap<String, int []> modelHashMap = new HashMap<String, int[]>();

		
		//List <String> models = getModelsList("Z:\\git\\mde-watermarking\\org.cea.lise.security.watermarking\\metamodel");
		String path = "/home/smartinez/git/RobustModelHashing/LSHForModels/samples/metamodels";
		List <String> models = HashComparator.getModelsList(path);

		System.out.println(models.toString());

		EObject root;
		//ModelHashing mh = new ModelHashing(50, 20, 5, 10, 150);
		//ModelHashing mh = new ModelHashing(50, 20, 5, 16, 80);;

		for (String metamodel: models) {
			System.out.println(metamodel);
			root = ModelUtil.doLoadXMIFromPath("samples" + File.separator + "metamodels" + File.separator + metamodel);
			EcoreMetamodelManager modelManager = new EcoreMetamodelManager(root);
			ModelHashing mh = new ModelHashing(100, 20, 5, 10, 80, modelManager);
			modelHashMap.put(metamodel, mh.createModelHash(root));
			System.out.println(Arrays.toString(mh.createModelHash(root)));
		}

		//\.[0-9]+,
		double [][] similarities = HashComparator.calculateSimilarities(modelHashMap);
		System.out.println(Arrays.deepToString(similarities));
		
		BandLSH blsh = new BandLSH(10000, 100, modelHashMap);
		blsh.doBands();

	}

}
