package tests;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.cea.lise.security.hashing.EcoreMetamodelManager;
import org.cea.lise.security.hashing.HashComparator;
import org.cea.lise.security.hashing.ModelHashing;
import org.cea.lise.security.hashing.ModelUtil;
import org.eclipse.emf.ecore.EObject;

import bandLSH.BandLSH;
import bandLSH.Collision;

public class TestLSHForMetamodelZoo {

	public static void main(String[] args) {
		System.out.println("Test Metamodel Assignments");
		
		//We store here the masks obtained for each metamodel
		HashMap<String, int []> modelHashMap = new HashMap<String, int[]>();

		
		//List <String> models = getModelsList("Z:\\git\\mde-watermarking\\org.cea.lise.security.watermarking\\metamodel");
		String path = "/home/smartinez/git/RobustModelHashing/LSHForModels/samples/EcoreZoo";
		List <String> models = HashComparator.getModelsList(path);

		System.out.println(models.toString());

		EObject root;
		//ModelHashing mh = new ModelHashing(50, 20, 5, 10, 150);
		//ModelHashing mh = new ModelHashing(50, 20, 5, 16, 80);;

		for (String metamodel: models) {
			//System.out.println(metamodel);
			root = ModelUtil.doLoadXMIFromPath("samples" + File.separator + "EcoreZoo" + File.separator + metamodel);
			int num = ModelUtil.numClasses(root);
			System.out.println(metamodel + " NumClasses>: " + num);
			if (num > 30) {
				EcoreMetamodelManager modelManager = new EcoreMetamodelManager(root);
				//We are taking 20 signatures and using 10 for the minhash. This gives higher similarities as it should.
				ModelHashing mh = new ModelHashing(100, 8, 8, 25, 80, modelManager);
				modelHashMap.put(metamodel, mh.createModelHash(root));
				//System.out.println(Arrays.toString(mh.createModelHash(root)));
			}
		}
		
		

		//\.[0-9]+,
		//double [][] similarities = HashComparator.calculateSimilarities(modelHashMap);
		//System.out.println(Arrays.deepToString(similarities));
		
		//System.out.println("Similarities mean =" + HashComparator.getMean(similarities));
		
		BandLSH blsh = new BandLSH(10000, 25, modelHashMap);
		HashMap<Set<String>, Collision> result = blsh.doBands();
		
		//System.out.println("Similarities col = " + blsh.calculateAverageSim(result, modelHashMap));
		
		System.out.println(result.size() + "," + blsh.calculateAverageSim(result, modelHashMap));
		blsh.printPairs(result, 50, true);
		
		//1, 2, 4, 5, 8, 10, 20, 25, 40, 50, 100, 200

	}

}
