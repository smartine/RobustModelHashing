package tests;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.cea.lise.security.hashing.AttributeFilter;
import org.cea.lise.security.hashing.EMFModelManager;
import org.cea.lise.security.hashing.EcoreMetamodelManager;
import org.cea.lise.security.hashing.HashComparator;
import org.cea.lise.security.hashing.ModelHashing;
import org.cea.lise.security.hashing.ModelUtil;
import org.eclipse.emf.ecore.EObject;

import bandLSH.BandLSH;
import bandLSH.Collision;

public class TestLSHforATL {
	public static void main(String[] args) {
		System.out.println("Test Test");
		
		//We store here the masks obtained for each metamodel
		HashMap<String, int []> modelHashMap = new HashMap<String, int[]>();
		
		String metamodelPath = "/home/smartinez/git/RobustModelHashing/org.cea.lise.security.hashing/TransModels/ATL.ecore";
		ModelUtil.registerMetamodel(metamodelPath);

		//List <String> models = getModelsList("Z:\\git\\mde-watermarking\\org.cea.lise.security.watermarking\\metamodel");
		String path = "/home/smartinez/git/RobustModelHashing/LSHForModels/samples/Trafo/2018";
		List <String> models = HashComparator.getModelsList(path);

		AttributeFilter af = new AttributeFilter();
		af.add("location");
		af.add("commentsBefore");
		af.add("commentsAfter");

		for (String metamodel: models) {
			System.out.println(metamodel);
			EObject root = ModelUtil.doLoadXMIFromPath("samples" + File.separator + "Trafo/2018" + File.separator + metamodel);
			EMFModelManager emfmodelmanager = new EMFModelManager(root, af);
			ModelHashing mh = new ModelHashing(100, 8, 5, 25, 80, emfmodelmanager);
			modelHashMap.put(metamodel, mh.createModelHash(root));
			//System.out.println(Arrays.toString(mh.createModelHash(root)));
		}

		double [][] similarities = HashComparator.calculateSimilarities(modelHashMap);
		System.out.println("Similarities mean =" + HashComparator.getMean(similarities));
		
		BandLSH blsh = new BandLSH(10000, 8, modelHashMap);
		HashMap<Set<String>, Collision> result = blsh.doBands();
		System.out.println(result.size() + "," + blsh.calculateAverageSim(result, modelHashMap));
		blsh.printPairs(result, 80, true);
		
		//1, 2, 4, 5, 8, 10, 20, 25, 40, 50, 100, 200
	    //Just iterate around divisors
		//for (int i=1; i <= 200; i++) 
        //    if (200 % i == 0) { 
        //    	BandLSH blsh = new BandLSH(10000, i, modelHashMap);
        //    	HashMap<Set<String>, Collision> result = blsh.doBands();
    		
            	//System.out.println("Similarities col = " + blsh.calculateAverageSim(result, modelHashMap));
    		
        //    	System.out.println(result.size() + "," + blsh.calculateAverageSim(result, modelHashMap));
            	//blsh.printPairs(result);
                //System.out.printf("%d ",i); 
        //    }

	}


}
