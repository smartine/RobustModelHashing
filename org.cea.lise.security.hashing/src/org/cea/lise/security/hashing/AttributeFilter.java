package org.cea.lise.security.hashing;

import java.util.HashSet;
import java.util.Set;

/**
 * Defines attributes to be ignored. For now, filtering is done by name. To be extended
 * to support types and expressions.
 * @author smartinez
 *
 */
public class AttributeFilter {
	Set<String> filteredAttributeNames = new HashSet<String>();
	
	public boolean add(String value) {
		return filteredAttributeNames.add(value);
	}

	public Set<String> getFilteredAttributeNames() {
		return filteredAttributeNames;
	}
}
