package org.cea.lise.security.hashing;

public enum ModelManagers {
	EcoreMetamodelManager,
	EMFModelManager
}
