package org.cea.lise.security.hashing;

public class BucketArray {
	int numBuckets;
	BucketItem [] bucketArray;
	
	
	public BucketArray(int numBuckets) {
		super();
		this.numBuckets = numBuckets;
		bucketArray = new BucketItem[numBuckets];
	}


	public int getNumBuckets() {
		return numBuckets;
	}


	public void setNumBuckets(int numBuckets) {
		this.numBuckets = numBuckets;
	}


	public BucketItem[] getBucketArray() {
		return bucketArray;
	}


	public void setBucketArray(BucketItem[] bucketArray) {
		this.bucketArray = bucketArray;
	}
	
	
	
}
