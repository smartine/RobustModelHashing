package org.cea.lise.security.hashing;

import java.io.File;
import java.lang.reflect.Array;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

public class HashComparator{
	
	public static void main(String[] args) {
		//We store here the masks obtained for each metamodel
		HashMap<String, int []> modelHashMap = new HashMap<String, int[]>();
		//List <String> models = getModelsList("Z:\\git\\mde-watermarking\\org.cea.lise.security.watermarking\\metamodel");
		List <String> models = getModelsList("/home/smartinez/git/RobustModelHashing/org.cea.lise.security.hashing/model");
		System.out.println(models.toString());
		EObject root;
		
		for (String metamodel: models) {
			System.out.println(metamodel);
			root = ModelUtil.doLoadXMIFromPath("model" + File.separator + metamodel);
			EcoreMetamodelManager modelManager = new EcoreMetamodelManager(root);
			ModelHashing mh = new ModelHashing(100, 20, 5, 10, 80, 6, modelManager);
			modelHashMap.put(metamodel, mh.createModelHash(root));
		}
		//double [][] similarities = calculateSimilarities(modelHashMap);
		//System.out.println(Arrays.deepToString(similarities));
	}
	
	/**
	 * Given a universe set E, the Hamming similarity for vectors is a function 
	 * HammingSim:E^n x E^n --> [0,1] that measures 
	 * the number of equals components, divided by the length of vectors. 
	 * @param hash1
	 * @param hash2
	 * @return
	 */
	public static double hammingSim(int [] hash1, int [] hash2) {
		double total = 0.0;
		for (int i = 0; i < hash1.length; i++) {
			if (hash1[i] == hash2[i])
				total++;
		}
		return (total/hash1.length)*100;
	}
	
	public static double [][] calculateSimilarities(TreeMap<String, int []> modelHashMap) {
		double [][] result = new double[modelHashMap.size()][modelHashMap.size()];
		Iterator it = modelHashMap.entrySet().iterator();
		int i = 0;
		while (it.hasNext()) {
			Map.Entry pair = (Map.Entry)it.next();
			int [] sourceMask = (int[]) pair.getValue();
			Iterator targetIt = modelHashMap.entrySet().iterator();
			int j = 0;
			while (targetIt.hasNext()) {
				Map.Entry targetPair = (Map.Entry)targetIt.next();
				int [] targetMask = (int[]) targetPair.getValue();
				Double val = hammingSim(sourceMask, targetMask);
				result[i][j] = val;
				if (val != 1.0) {
					//System.out.println( (String)pair.getKey() + "-" + (String)targetPair.getKey() + ":" + val);
					//System.out.println(val*100);
				}
				j++;
			}
			i++;
		}
		return  result;
	}
	
	public static double [][] calculateUniqueSimilarities(TreeMap<String, int []> modelHashMap) {
		double [][] result = new double[modelHashMap.size()][modelHashMap.size()];
		Iterator it = modelHashMap.entrySet().iterator();
		int i = 0;
		while (it.hasNext()) {
			Map.Entry pair = (Map.Entry)it.next();
			int [] sourceMask = (int[]) pair.getValue();
			Iterator targetIt = modelHashMap.entrySet().iterator();
			int j = 0;
			while (targetIt.hasNext()) {
				Map.Entry targetPair = (Map.Entry)targetIt.next();
				//if (pair != targetPair) {
					int [] targetMask = (int[]) targetPair.getValue();
					Double val = hammingSim(sourceMask, targetMask);
					result[i][j] = val;
					//if (val != 1.0) {
					System.out.println( (String)pair.getKey() + "-" + (String)targetPair.getKey() + ":" + val);
					//}
				//}
					
				j++;
			}
			i++;
		}
		return  result;
	}
	
	public static double getMean(double [][] similarities) {
		double sum = 0.0;
		int lenght = similarities.length;
		
		for (int i = 0; i < lenght; i++) {
			for (int j = 0; j < lenght; j++) {
				sum += similarities[i][j];
			}
		}
		return sum/(lenght*lenght);
	}
	
	/**
	 * List files in a Folder
	 * @param path
	 * @return
	 */
	public static List<String> getModelsList(String path){
		List<String> results = new ArrayList<String>();
		File[] files = new File(path).listFiles();
		//If this pathname does not denote a directory, then listFiles() returns null. 

		for (File file : files) {
			if (file.isFile()) {
				results.add(file.getName());
			}
		}
		return results;
	}
	
	/**
	 * Counts the number of metaclasses in a metamodel. There is probably some better way of doing this
	 * @param root
	 * @return
	 */
	public static int numClasses(EObject root) {
		int total = 0;
		TreeIterator<EObject> contents = root.eAllContents();
		while (contents.hasNext()) {
			EObject next = contents.next();
			if (next instanceof EClass) {
				total++;
			}
		}
		return total;
	}

}
