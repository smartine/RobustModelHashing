package org.cea.lise.security.hashing;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.Vector;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.DynamicEObjectImpl;
import org.eclipse.emf.ecore.impl.EClassImpl;

public class EMFModelManager extends AbstractModelManager {
	
	EObject root;
	Random generator = new Random(12345);
	AttributeFilter af;
	
	public EMFModelManager(EObject root, AttributeFilter afilter) {
		super();
		this.root = root;
		this.af = afilter;
	}
	
	public void setAttributeFilter(AttributeFilter af) {
		this.af = af;
	}

	@Override
	public Vector<Fragment> createFragments(int numFragments, int fragmentSize) {
		Vector<Fragment> fragmentVector = new Vector <Fragment>();
		List<EObject> availableClasses = ModelUtil.getModelElements(root);
				
		for (int i=0; i < numFragments; i++) {
			Fragment currentFragment = new Fragment(new HashSet<EObject>());
			EObject currentCenter;
			
			//We are going to take centers at random. This is ok as long as we take many centers and then
			//we classify them by their content. Alternatively, we could just take every element as center
			currentCenter = availableClasses.get(generator.nextInt(availableClasses.size()));
			currentFragment.getObjects().add(currentCenter);
			currentFragment.getObjects().addAll(getNneighbours(fragmentSize - 1, currentCenter));
			fragmentVector.add(currentFragment);
			//System.out.println(currentFragment);
		}
		
		return fragmentVector;
	}
	
	public static Set<EObject> getNneighbours(int n, EObject center){
		Set<EObject> neighbours =  new LinkedHashSet<EObject>();
	
		//We add the container to the summary
		if (center.eContainer() != null &&
				!center.eContainer().eClass().getName().equals("EPackage") )
			neighbours.add(center.eContainer());
		
		//We get all the contents
		neighbours.addAll(center.eContents());
		
		//We add (non-containment) references.
		EList<EReference> refs = center.eClass().getEReferences();
		for (EReference ref : refs) {
			if (!ref.isContainment()) {
				Object o = center.eGet(ref);
				if (o instanceof DynamicEObjectImpl || o instanceof EClassImpl) {
					neighbours.add((EObject)o);
				}
			}
		}
		
		
//		Collection<Setting> references = EcoreUtil.UsageCrossReferencer.find(center, center.eResource());
//				for (Setting setting : references) {
//					EObject source = setting.getEObject();
//					if (source instanceof EReferenceImpl) {
//						if (source.eContainer() instanceof EClass) {
//							EClass ec = (EClass) source.eContainer();
//							if (!usedClasses.contains(ec.getName()))
//							neighbours.add(ec);
//						}
//					}
//				}
		
		if (neighbours.size() == n) {
			//cleanSet(usedClasses, neighbours);
			return neighbours;
		}else if (neighbours.size() > n){
			Set<EObject> neighboursCopy =  new LinkedHashSet<EObject>();
			Iterator<EObject> it = neighbours.iterator();
			while(it.hasNext() && (neighboursCopy.size() < n) ) {
				neighboursCopy.add(it.next());
			}
			//cleanSet(usedClasses, neighboursCopy);
			return neighboursCopy;
		}else {
			//we need to iterate through the neighbours
			Iterator<EObject> it = neighbours.iterator();
			int size = neighbours.size();
			int missing = n-size;
			Set<EObject> neighboursCopy =  new LinkedHashSet<EObject>();
			while (missing > 0 && it.hasNext()){
				neighboursCopy.addAll(getNneighbours(n-size, it.next()));
				missing = missing - neighboursCopy.size();
			}
			Iterator<EObject> itt = neighboursCopy.iterator();
			while(itt.hasNext() && (neighbours.size() < n) ) {
				neighbours.add(itt.next());
			}
			//cleanSet(usedClasses, neighbours);
			return neighbours;
		}
	}


	@Override
	public Set<String> calculateSummary(Object o) {
		Set<String> set = new HashSet<String>();
		EObject obj = (EObject)o;
		EClass c = obj.eClass();
		
		set.add(c.getName());
		
		//We add the values of the object attributes to the summary.
		EList<EAttribute> atts = c.getEAllAttributes();
		for (EAttribute att : atts) {
			Object val = obj.eGet(att, true);
			if (val != null && !af.getFilteredAttributeNames().contains(att.getName()))
				set.add(val.toString());
		}
		return set;	 
	}

	@Override
	public Set<String> createFragmentSummary(Set<?> s) {
		EObject[] elementsArray = new EObject[s.size()];
		s.toArray(elementsArray);
		Set<String> fragmentSummary  = new HashSet<String>();
		
		for (EObject e : elementsArray) {
				fragmentSummary.addAll(calculateSummary(e));
		}
		return fragmentSummary;
	}
}
