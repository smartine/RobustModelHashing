package org.cea.lise.security.hashing;

import org.eclipse.emf.ecore.EObject;

public class ModelManagerFactory {
	
	
	public ModelManagerFactory() {
		super();
	}

	public AbstractModelManager createManager(EObject root, ModelManagers mm, AttributeFilter af) {
		switch (mm) {
		case EcoreMetamodelManager:
			return new EcoreMetamodelManager(root);
		case EMFModelManager:
			return new EMFModelManager(root, af);
		default:
			throw new IllegalArgumentException("Unexpected value: " + mm);
		}
	}

}
