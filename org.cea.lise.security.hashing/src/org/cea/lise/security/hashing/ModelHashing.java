package org.cea.lise.security.hashing;

import java.util.Set;
import java.util.Vector;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.cea.lise.security.hashing.minhash.MinHash;
import org.eclipse.emf.ecore.EObject;

public class ModelHashing {
	
	int numBuckets;
	int minhashk;
	int bandsize;
	int numHashes;
	int numFragments;
	int fragmentSize;
	AbstractModelManager modelManager;
	
	/**
	 * 
	 * @param numBuckets Number of buckets for each band
	 * @param minhashk Size of the minhash signature
	 * @param bandsize Size of the band
	 * @param numHashes Number of signatures taken from the bands to create the hash
	 * @param fragmentSize size of the fragments
	 * @param model manager
	 */
	public ModelHashing(int numBuckets, int minhashk, int bandsize, 
			int numHashes, int numFragments, int fragmentSize, AbstractModelManager modelManager) {
		super();
		this.numBuckets = numBuckets;
		this.minhashk = minhashk;
		this.bandsize = bandsize;
		this.numHashes = numHashes;
		this.numFragments = numFragments;
		this.modelManager = modelManager;
		this.fragmentSize = fragmentSize;
	}
	
	public int[] createModelHash(EObject root) {
		MinHash minh = new MinHash(minhashk);
		SignatureClassifier sc = new SignatureClassifier(numBuckets, minhashk, bandsize);
		Vector<Fragment> fragmentVector = modelManager.createFragments(numFragments, fragmentSize);
		for (int i = 0; i < numFragments; i++) {
			Fragment currentFragment = fragmentVector.get(i);
			Set<String> fSummary = modelManager.createFragmentSummary(currentFragment.getObjects());
			Signature sig = new Signature(minh.minHashSig(fSummary));
			sc.fillBands(sig, bandsize);
		}
		
		int [][] signatures = sc.getNSignaturesFromBands(numHashes);
		return flatten(signatures);
	}

	public int[] flatten(int[][] arr) {
		   return Stream.of(arr)
		      .flatMapToInt(row -> IntStream.of(row))
		      .toArray();
		}
}
