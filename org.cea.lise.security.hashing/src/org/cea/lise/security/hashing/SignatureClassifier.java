package org.cea.lise.security.hashing;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Random;


public class SignatureClassifier {
	
	int numBuckets;
	int signatureSize;
	int numbands;
	HashMap<Integer, Integer[]> bands = new HashMap<Integer, Integer[]>(); 
	BucketArray [] BucketListsArray;
	
	
	public SignatureClassifier(int numBuckets, int signatureSize, int bandSize) {
		super();
		this.numBuckets = numBuckets;
		this.signatureSize = signatureSize;
		numbands = signatureSize / bandSize;
		//We need a bucketList for each band
		BucketListsArray = new BucketArray[numbands];
		for (int i = 0; i < numbands; i++) {
			BucketListsArray[i] = new BucketArray(numBuckets);
		}
	}

	public void fillBands(Signature signature, int bandSize) {
		for (int i = 0; i < numbands; i++) {
			int[] band = signature.getBand(i*bandSize, ((i+1)*bandSize)); 
			int position = Math.floorMod(Arrays.hashCode(band), numBuckets);
			BucketArray hashList = BucketListsArray[i];
			if (hashList.bucketArray[position] == null) {
				//the bucket is empty
				List<Signature> itemList = new ArrayList();
				itemList.add(signature);
				BucketItem item = new BucketItem(itemList);
				hashList.bucketArray[position] = item;
			}else {
				//this is a collision
				BucketItem item = hashList.bucketArray[position];
				item.itemList.add(signature);
			}
		}	
	}
	
	public int[][] getNSignaturesFromBands(int n) {
		int [][] signatures = new int[n][signatureSize];
		Random chooseBand = new Random(998877);
		Random generator = new Random(81927383);
		
		int count = 0;
		while (count < n) {
			int band = chooseBand.nextInt(numbands);
			int val = generator.nextInt(numBuckets);
			BucketArray ba = BucketListsArray[band];
			if (ba.getBucketArray()[val] != null) {
				Signature s = ba.getBucketArray()[val].getItemList().get(0);
				signatures[count] = s.getSignature();
				count++;
			}else {
				while (ba.getBucketArray()[val] == null) {
					val = Math.floorMod(val+1, numBuckets);
					if (ba.getBucketArray()[val] != null) {
						Signature s = ba.getBucketArray()[val].getItemList().get(0);
						signatures[count] = s.getSignature();
						count++;
					}
				}
			}
		}
		return signatures;
	}
}
