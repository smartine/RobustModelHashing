package org.cea.lise.security.hashing;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.BasicExtendedMetaData;
import org.eclipse.emf.ecore.util.ExtendedMetaData;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

public class ModelUtil {

	public static EObject doLoadXMIFromPath(String Path) {
		Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
		Map<String, Object> m = reg.getExtensionToFactoryMap();
		m.put("ecore", new XMIResourceFactoryImpl());
		m.put("xmi", new XMIResourceFactoryImpl());
		ResourceSet resSet = new ResourceSetImpl();
		Resource resource = resSet.getResource(URI.createURI(Path), true);
		try {
			resource.load(m);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//FIXME:  we suppose only one model root. This is a very dirty solution.
		//EPackage p = (EPackage) resource.getContents().get(0);
		
		EList<EObject> packages = resource.getContents();
		for (EObject o : packages) {
			//EObject eObject = r.getContents().get(0);
			if (o instanceof EPackage) {
				EPackage p = (EPackage)o;
				if (!p.getName().equals("PrimitiveTypes")) 
					return o;
			}
			else {
				return resource.getContents().get(0);
			}
				
		}
		return null;
	}
		
		
	
	public static int registerMetamodel(String Path) {
		int registeredPackages = 0;
		Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put(
		    "ecore", new EcoreResourceFactoryImpl());
		Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put(
			    "xmi", new EcoreResourceFactoryImpl());

		ResourceSet rs = new ResourceSetImpl();
		// enable extended metadata
		final ExtendedMetaData extendedMetaData = new BasicExtendedMetaData(rs.getPackageRegistry());
		rs.getLoadOptions().put(XMLResource.OPTION_EXTENDED_META_DATA,
		    extendedMetaData);

		Resource r = rs.getResource(URI.createURI(Path), true);
		EList<EObject> packages = r.getContents();
		for (EObject o : packages) {
			//EObject eObject = r.getContents().get(0);
			if (o instanceof EPackage) {
				EPackage p = (EPackage)o;
				EPackage.Registry.INSTANCE.put(p.getNsURI(), p);
				registeredPackages++;
			}
		}
		return registeredPackages;
	}

	/* Counts the number of metaclasses in a metamodel. There is probably some better way of doing this
	 * @param root
	 * @return
	 */
	public static int numClasses(EObject root) {
		int total = 0;
		TreeIterator<EObject> contents = root.eAllContents();
		while (contents.hasNext()) {
			EObject next = contents.next();
			if (next instanceof EClass) {
				total++;
			}
		}
		return total;
	}
	
	public static TreeMap<String, EObject> getElements(EObject root) {
		TreeMap<String, EObject> hm = new TreeMap<String, EObject>();
		TreeIterator<EObject> contents = root.eAllContents();
		while (contents.hasNext()) {
		      EObject next = contents.next();	   
		      if (next instanceof EClass) {
		    	  EClass ec = (EClass) next;
		    	  hm.put(ec.getName(), next);
		      }
		}
		return hm;
	}
	
	public static List<EObject> getModelElements(EObject root) {
		List<EObject> hm = new ArrayList<EObject>();
		TreeIterator<EObject> contents = root.eAllContents();
		while (contents.hasNext()) {
		      EObject next = contents.next();	
		      hm.add(next);
		}
		return hm;
	}
}
