package org.cea.lise.security.hashing;

import java.util.Set;
import java.util.Vector;

public abstract class AbstractModelManager {
	
	public abstract Vector<Fragment> createFragments(int numFragments, int fragmentSize);
	public abstract Set<String> calculateSummary(Object o);
	public abstract Set<String> createFragmentSummary(Set<?> s);

}
