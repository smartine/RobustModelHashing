package org.cea.lise.security.hashing;

import java.util.Set;

import org.eclipse.emf.ecore.EObject;

public class Fragment {
	Set<EObject> objects;

	public Set<EObject> getObjects() {
		return objects;
	}

	public void setObjects(Set<EObject> objects) {
		this.objects = objects;
	}

	public Fragment(Set<EObject> objects) {
		super();
		this.objects = objects;
	}
}
