package org.cea.lise.security.hashing;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;
import java.util.Vector;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature.Setting;
import org.eclipse.emf.ecore.impl.EReferenceImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;

public class EcoreMetamodelManager extends AbstractModelManager {
	
	public EcoreMetamodelManager(EObject root) {
		super();
		this.root = root;
	}

	EObject root;

	@Override
	public Vector<Fragment> createFragments(int numFragments, int fragmentSize) {
		Vector<Fragment> fragmentVector = new Vector <Fragment>();
		TreeMap<String, EObject> hm1 = ModelUtil.getElements(root);
		Set<String> usedClasses =  new HashSet<String>();

		//Random generator = new Random(12345);
		List<String> availableClasses = new ArrayList<String>(hm1.keySet());
				
		for (int i=0; i < numFragments; i++) {
			
			Fragment currentFragment = new Fragment(new HashSet<EObject>());
			//Set<EObject> currentFragment =  new HashSet<EObject>();
			EObject currentCenter;
			
			String className;
			if (i < availableClasses.size()) {
				className = availableClasses.get(i);
			}else {
				className = availableClasses.get(i % availableClasses.size());
			}
			currentCenter = hm1.get(className);
			usedClasses.add(className);
			currentFragment.getObjects().add(currentCenter);
			currentFragment.getObjects().addAll(getNneighbours(fragmentSize - 1, currentCenter, usedClasses));
			
			fragmentVector.add(currentFragment);
		}
		
		return fragmentVector;

	}

	@Override
	public Set<String> calculateSummary(Object c) {
		Set<String> set = new HashSet<String>();
		
		EClass ec = (EClass)c;
		
		//We add the name to the summary
		set.add(ec.getName());

		//We add the attributes
		for (EAttribute att : ec.getEAttributes()) {
			set.add(att.getName());
		}

		//We add all the operations
		for (EOperation opr : ec.getEOperations()) {
			set.add(opr.getName());
		}

		return set;	 
	}

	@Override
	public Set<String> createFragmentSummary(Set<?> s) {
		EObject[] elementsArray = new EObject[s.size()];
		s.toArray(elementsArray);
		Set<String> fragmentSummary  = new HashSet<String>();
		
		for (EObject e : elementsArray) {
			if (e instanceof EClass)
				fragmentSummary.addAll(calculateSummary((EClass) e));
		}
		return fragmentSummary;
	}
	
	public static Set<EObject> getNneighbours(int n, EObject center, Set<String> usedClasses){
		Set<EObject> neighbours =  new LinkedHashSet<EObject>();
	
		//We add the super types to the summary
		for (EClass item : ((EClass) center).getESuperTypes()) {
			if (!usedClasses.contains(item.getName()))
				neighbours.add(item);
		}

		//We add the container to the summary
		if (!center.eContainer().eClass().getName().equals("EPackage") )
			if (!usedClasses.contains(center.eContainer().eClass().getName()))
			neighbours.add(center.eContainer().eClass());
	

		//We add all the references
		for (EReference ref : ((EClass) center).getEReferences()) {
			String name = ref.getEType().getName();
			if (name != null && !usedClasses.contains(name))
				neighbours.add(ref.getEType());
		}
		
		Collection<Setting> references = EcoreUtil.UsageCrossReferencer.find(center, center.eResource());
				for (Setting setting : references) {
					EObject source = setting.getEObject();
					if (source instanceof EReferenceImpl) {
						if (source.eContainer() instanceof EClass) {
							EClass ec = (EClass) source.eContainer();
							if (!usedClasses.contains(ec.getName()))
							neighbours.add(ec);
						}
					}
				}
		
		if (neighbours.size() == n) {
			//cleanSet(usedClasses, neighbours);
			return neighbours;
		}else if (neighbours.size() > n){
			Set<EObject> neighboursCopy =  new LinkedHashSet<EObject>();
			Iterator<EObject> it = neighbours.iterator();
			while(it.hasNext() && (neighboursCopy.size() < n) ) {
				neighboursCopy.add(it.next());
			}
			//cleanSet(usedClasses, neighboursCopy);
			return neighboursCopy;
		}else {
			//we need to iterate through the neighbours
			Iterator<EObject> it = neighbours.iterator();
			int size = neighbours.size();
			int missing = n-size;
			Set<EObject> neighboursCopy =  new LinkedHashSet<EObject>();
			while (missing > 0 && it.hasNext()){
				neighboursCopy.addAll(getNneighbours(n-size, it.next(), usedClasses));
				missing = missing - neighboursCopy.size();
			}
			//neighbours.addAll(neighboursCopy);
			Iterator<EObject> itt = neighboursCopy.iterator();
			while(itt.hasNext() && (neighbours.size() < n) ) {
				neighbours.add(itt.next());
			}
			//cleanSet(usedClasses, neighbours);
			return neighbours;
		}
	}

}
