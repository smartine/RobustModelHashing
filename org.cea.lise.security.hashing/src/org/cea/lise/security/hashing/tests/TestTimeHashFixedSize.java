package org.cea.lise.security.hashing.tests;

import java.util.HashMap;

import org.cea.lise.security.hashing.AbstractModelManager;
import org.cea.lise.security.hashing.AttributeFilter;
import org.cea.lise.security.hashing.ModelHashing;
import org.cea.lise.security.hashing.ModelManagerFactory;
import org.cea.lise.security.hashing.ModelManagers;
import org.cea.lise.security.hashing.ModelUtil;
import org.eclipse.emf.ecore.EObject;

public class TestTimeHashFixedSize {
	
	public static void main(String[] args) {
		
		HashMap <String, Integer> RHValues = new HashMap<String, Integer>();
		ModelManagerFactory mfactory = new ModelManagerFactory();
		
		int numhashes = 40;
		int numfragments = 100; 
		int fragmentsize = 5; 
		
		RHValues.put("numbuckets", 200);
		RHValues.put("minhashk", 20);
		RHValues.put("bandsize", 5);
		RHValues.put("numhashes", numhashes);
		RHValues.put("numfragments", numfragments);
		RHValues.put("fragmentsize", fragmentsize);
	
		String metamodelPath = "/home/smartinez/Work/Eclipses/eclipse/workspaces/wRobustHashing/Tests/tests/PetriNet.ecore";
		String path = "/home/smartinez/Work/Eclipses/eclipse/workspaces/wRobustHashing/fr.inria.atlanmod.instantiator/PetriNet/result13_500000.xmi";
	
		if (!metamodelPath.isEmpty()) {
			ModelUtil.registerMetamodel(metamodelPath);
		}
		
		EObject root;
		
		
		System.out.println("Name, time");
		for (int i = 0; i <= 10; i++ ) {
			//System.out.println(model);
			
			
			root = ModelUtil.doLoadXMIFromPath(path);
			AbstractModelManager modelManager = mfactory.createManager(root, ModelManagers.EMFModelManager, new AttributeFilter());
			
			
			
			RHValues.put("numhashes", numhashes);
			RHValues.put("numfragments", numfragments);
			RHValues.put("fragmentsize", fragmentsize);
			
			//numhashes = numhashes + 10;
			//numfragments = numfragments; 
			fragmentsize = fragmentsize + 1000; 
			
			long tStart = System.currentTimeMillis();
			ModelHashing mh = new ModelHashing(RHValues.get("numbuckets"), 
												RHValues.get("minhashk"),
												RHValues.get("bandsize"),
												RHValues.get("numhashes"),
												RHValues.get("numfragments"),
												RHValues.get("fragmentsize"),
												modelManager);
			int [] modelHash = mh.createModelHash(root);
			long tEnd = System.currentTimeMillis();
			long tDelta = tEnd - tStart;
			double elapsedSeconds = tDelta / 1000.0;
			System.out.println(elapsedSeconds + ", ");
			//System.out.println(Arrays.toString(modelHash));
		}
	}

}
