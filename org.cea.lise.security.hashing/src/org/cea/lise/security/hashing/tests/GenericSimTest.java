package org.cea.lise.security.hashing.tests;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;

import org.cea.lise.security.hashing.AbstractModelManager;
import org.cea.lise.security.hashing.AttributeFilter;
import org.cea.lise.security.hashing.HashComparator;
import org.cea.lise.security.hashing.ModelHashing;
import org.cea.lise.security.hashing.ModelManagerFactory;
import org.cea.lise.security.hashing.ModelManagers;
import org.cea.lise.security.hashing.ModelUtil;
import org.eclipse.emf.ecore.EObject;

/**
 * Calculates similarities between a set of models in a given folder.
 * @author smartinez
 *
 */
public class GenericSimTest {
	TreeMap<String, int []> modelHashMap = new TreeMap<String, int[]>();
	ModelManagerFactory mfactory = new ModelManagerFactory();
	AttributeFilter af;
	
	public GenericSimTest() {
		super();
	}
	
	public void setAttributeFilter(AttributeFilter af) {
		this.af = af;
	}

	private double [][] calculateSimilarities(String metamodelPath, String modelsPath, ModelManagers mm, HashMap <String, Integer> RHValues){
		List <String> models = HashComparator.getModelsList(modelsPath);
		java.util.Collections.sort(models);
		EObject root;
		for (String model: models) {
			//System.out.println(model);
			root = ModelUtil.doLoadXMIFromPath(modelsPath + File.separator + model);
			AbstractModelManager modelManager = mfactory.createManager(root, mm, af);
			
			ModelHashing mh = new ModelHashing(RHValues.get("numbuckets"), 
												RHValues.get("minhashk"),
												RHValues.get("bandsize"),
												RHValues.get("numhashes"),
												RHValues.get("numfragments"),
												RHValues.get("fragmentsize"),
												modelManager);
			modelHashMap.put(model, mh.createModelHash(root));
		}
		return HashComparator.calculateUniqueSimilarities(modelHashMap);
	}
	
	public double [][] run(String metamodelPath, String modelsPath, ModelManagers mm, HashMap <String, Integer> RHValues) {
		if (!metamodelPath.isEmpty()) {
			ModelUtil.registerMetamodel(metamodelPath);
		}
		return calculateSimilarities(metamodelPath, modelsPath, mm, RHValues);
	}
	
	public void printV(double [][] similarities) {
		for (int i = 0; i < similarities.length; i++) {
			for (int j = 0; j < similarities[i].length; j++)
				//if (i != j) {
					System.out.print((int)similarities[i][j] + ", ");
				//}
			System.out.println("Model " + i);
		}
	}

}
