package org.cea.lise.security.hashing.tests.emfcompare;

import java.io.File;
import java.util.List;

import org.cea.lise.security.hashing.HashComparator;
import org.cea.lise.security.hashing.ModelUtil;
import org.eclipse.emf.compare.Comparison;
import org.eclipse.emf.compare.EMFCompare;
import org.eclipse.emf.compare.scope.DefaultComparisonScope;
import org.eclipse.emf.compare.scope.IComparisonScope;
import org.eclipse.emf.ecore.EObject;

public class GenericDiffsTest {

	public GenericDiffsTest() {
		super();
	}
	
	public double [][] run (String metamodelPath, String modelsPath){
		if (!metamodelPath.isEmpty()) {
			ModelUtil.registerMetamodel(metamodelPath);
		}
		return calculateSimilarities(metamodelPath, modelsPath);
	}
	
	private double [][] calculateSimilarities(String metamodelPath, String modelsPath){
		EObject root1, root2;
		List <String> models = HashComparator.getModelsList(modelsPath);
		java.util.Collections.sort(models);
		double [][] diffs = new double[models.size()][models.size()];
		String m1, m2;
		for (int i = 0; i < models.size(); i++) {
			m1 = models.get(i);
			for (int j = i; j < models.size(); j++) {
				//System.out.println(metamodel);
				//We are not interested in comparing the same elements
				m2 = models.get(j);
				if (m1.compareTo(m2) != 0) {
					root1 = ModelUtil.doLoadXMIFromPath(modelsPath + File.separator + m1);
					root2 = ModelUtil.doLoadXMIFromPath(modelsPath + File.separator + m2);

					IComparisonScope scope = new DefaultComparisonScope(root1, root2, null);
					Comparison comparison = EMFCompare.builder().build().compare(scope);

					List<org.eclipse.emf.compare.Diff> differences = comparison.getDifferences();
					//System.out.println("M1: " + m1 + " M2: " + m2 + " |DIF = " + differences.size());
					//System.out.println(differences.size());
					differences.size();
					diffs[i][j] = differences.size();
				}
			}
		}
		return diffs;
	}
	
	public void printV(double [][] differences) {
		for (int i = 0; i < 1; i++) {
			for (int j = i; j < differences.length; j++)
				if (i != j) {
					System.out.println(differences[i][j]);
				}
		}
	}
	

}
