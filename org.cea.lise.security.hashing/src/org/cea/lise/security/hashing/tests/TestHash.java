package org.cea.lise.security.hashing.tests;

import java.io.File;
import java.util.Arrays;

import org.cea.lise.security.hashing.EcoreMetamodelManager;
import org.cea.lise.security.hashing.ModelHashing;
import org.cea.lise.security.hashing.ModelUtil;
import org.eclipse.emf.ecore.EObject;

public class TestHash {
	
	
	
	
	// First of all, we have to open the model, then, create N partitions, then, call minhash on it and finally, quantize it
	public static void main(String[] args) {
		
		EObject root = ModelUtil.doLoadXMIFromPath("model" + File.separator + "OpenConf.owl.ecore");
		EcoreMetamodelManager modelManager = new EcoreMetamodelManager(root);
		ModelHashing mh = new ModelHashing(100, 10, 5, 10, 20, 6, modelManager);
		long tStart = System.currentTimeMillis();
		int [] modelHash = mh.createModelHash(root);
		long tEnd = System.currentTimeMillis();
		long tDelta = tEnd - tStart;
		double elapsedSeconds = tDelta / 1000.0;
		System.out.println("Time: " + elapsedSeconds);
		System.out.println(Arrays.toString(modelHash));
	}
	
	
	
	
		
}
