package org.cea.lise.security.hashing.tests;

import java.util.HashMap;

import org.cea.lise.security.hashing.ModelManagers;

public class TestSimForMetamodels {
	
	public static void main(String[] args) {
		
		HashMap <String, Integer> RHValues = new HashMap<String, Integer>();
		
		RHValues.put("numbuckets", 500);
		RHValues.put("minhashk", 30);
		RHValues.put("bandsize", 6);
		RHValues.put("numhashes", 100);
		RHValues.put("numfragments", 200);
		RHValues.put("fragmentsize", 10);
		
		String path = "/home/smartinez/Work/GitRepos/RobustModelHashing/org.cea.lise.security.hashing/metamodelsGithub";
		GenericSimTest test = new GenericSimTest();
		double [][] similarities = test.run("", path, ModelManagers.EcoreMetamodelManager, RHValues);
		test.printV(similarities);
	}
}
