package org.cea.lise.security.hashing.tests;

import java.util.HashMap;

import org.cea.lise.security.hashing.AttributeFilter;
import org.cea.lise.security.hashing.ModelManagers;

public class TestSimForModels {
	
	public static void main(String[] args) {
		
		HashMap <String, Integer> RHValues = new HashMap<String, Integer>();
	
		RHValues.put("numbuckets", 500);
		RHValues.put("minhashk", 30);
		RHValues.put("bandsize", 5);
		RHValues.put("numhashes", 100);
		RHValues.put("numfragments", 200);
		RHValues.put("fragmentsize", 10);
		
		//String metamodelPath = "/home/smartinez/Work/GitRepos/RobustModelHashing/org.cea.lise.security.hashing/TransModels/ATL.ecore";
	
		String metamodelPath = "/home/smartinez/Work/Eclipses/eclipse/workspaces/wRobustHashing/Tests/tests/PetriNet.ecore";
		String path = "/home/smartinez/Work/GitRepos/RobustModelHashing/org.cea.lise.security.hashing/PetriModels";
		//String path = "/home/smartinez/Work/GitRepos/mde-watermarking/org.cea.lise.security.watermarking.mutation/TrafoRepo";
		GenericSimTest test = new GenericSimTest();
		test.setAttributeFilter(new AttributeFilter());
		double [][] similarities = test.run(metamodelPath, path, ModelManagers.EMFModelManager, RHValues);
		test.printV(similarities);
		
		//Testting ATL transformations
//		AttributeFilter af = new AttributeFilter();
//		af.add("location");
//		af.add("commentsBefore");
//		af.add("commentsAfter");
//		metamodelPath = "/home/smartinez/Work/GitRepos/RobustModelHashing/org.cea.lise.security.hashing/TransModels/ATL.ecore";
//		path = "/home/smartinez/Work/GitRepos/RobustModelHashing/org.cea.lise.security.hashing/TransModels/Transformations";
//		test = new GenericSimTest();
//		test.setAttributeFilter(af);
//		similarities = test.run(metamodelPath, path, ModelManagers.EMFModelManager, RHValues);
//		test.printV(similarities);
	}
}
