package org.cea.lise.security.hashing.tests.emfcompare;

public class TestDiffsForMetamodels {
	
	static String metamodelPath = "";
	static String path = "/home/smartinez/Work/GitRepos/RobustModelHashing/org.cea.lise.security.hashing/mutants/UML2";
	
	public static void main(String[] args) {
	
		GenericDiffsTest testEMF = new GenericDiffsTest();
		double [][] differences = testEMF.run(metamodelPath, path);
		testEMF.printV(differences);
	}
}
