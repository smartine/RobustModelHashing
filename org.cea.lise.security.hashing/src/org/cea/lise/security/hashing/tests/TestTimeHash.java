package org.cea.lise.security.hashing.tests;

import java.io.File;
import java.text.Collator;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.cea.lise.security.hashing.AbstractModelManager;
import org.cea.lise.security.hashing.AttributeFilter;
import org.cea.lise.security.hashing.HashComparator;
import org.cea.lise.security.hashing.ModelHashing;
import org.cea.lise.security.hashing.ModelManagerFactory;
import org.cea.lise.security.hashing.ModelManagers;
import org.cea.lise.security.hashing.ModelUtil;
import org.eclipse.emf.ecore.EObject;

public class TestTimeHash {
	
	public static void main(String[] args) {
		
		HashMap <String, Integer> RHValues = new HashMap<String, Integer>();
		ModelManagerFactory mfactory = new ModelManagerFactory();
		
		RHValues.put("numbuckets", 200);
		RHValues.put("minhashk", 20);
		RHValues.put("bandsize", 5);
		RHValues.put("numhashes", 80);
		RHValues.put("numfragments", 200);
		RHValues.put("fragmentsize", 10);
	
		String metamodelPath = "/home/smartinez/Work/Eclipses/eclipse/workspaces/wRobustHashing/Tests/tests/PetriNet.ecore";
		String path = "/home/smartinez/Work/Eclipses/eclipse/workspaces/wRobustHashing/fr.inria.atlanmod.instantiator/PetriNet";
	
		if (!metamodelPath.isEmpty()) {
			ModelUtil.registerMetamodel(metamodelPath);
		}
		
		List <String> models = HashComparator.getModelsList(path);
		java.util.Collections.sort(models);
		EObject root;
		System.out.println("Name, time");
		for (String model: models) {
			//System.out.println(model);
			
			long tStart = System.currentTimeMillis();
			root = ModelUtil.doLoadXMIFromPath(path + File.separator + model);
			AbstractModelManager modelManager = mfactory.createManager(root, ModelManagers.EMFModelManager, new AttributeFilter());
			long tEnd = System.currentTimeMillis();
			long tDelta = tEnd - tStart;
			double elapsedSeconds = tDelta / 1000.0;
			//long tStart = System.currentTimeMillis();
			
			ModelHashing mh = new ModelHashing(RHValues.get("numbuckets"), 
												RHValues.get("minhashk"),
												RHValues.get("bandsize"),
												RHValues.get("numhashes"),
												RHValues.get("numfragments"),
												RHValues.get("fragmentsize"),
												modelManager);
			int [] modelHash = mh.createModelHash(root);
			//long tEnd = System.currentTimeMillis();
			//long tDelta = tEnd - tStart;
			//double elapsedSeconds = tDelta / 1000.0;
			System.out.println(elapsedSeconds);
			//System.out.println(Arrays.toString(modelHash));
		}
	}

}
