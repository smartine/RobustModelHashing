package org.cea.lise.security.hashing.tests;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.Vector;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.cea.lise.security.hashing.AbstractModelManager;
import org.cea.lise.security.hashing.EcoreMetamodelManager;
import org.cea.lise.security.hashing.Fragment;
import org.cea.lise.security.hashing.HashComparator;
import org.cea.lise.security.hashing.ModelUtil;
import org.cea.lise.security.hashing.Signature;
import org.cea.lise.security.hashing.minhash.MinHash;
import org.eclipse.emf.ecore.EObject;

public class TestK {
	
	static DecimalFormat df = new DecimalFormat("#.###");

	// First of all, we have to open the model, then, create N partitions, then, call minhash on it and finally, quantize it
	public static void main(String[] args) {
		
		EObject root = ModelUtil.doLoadXMIFromPath("model" + File.separator + "OpenConf.owl.ecore");
		EcoreMetamodelManager modelManager = new EcoreMetamodelManager(root);
		createModelHash(root, 100, 5, 10, 20, 20, modelManager);
	}
	
	public static void createModelHash(EObject root, int numBuckets, int bandsize, int numHashes, int numFragments, int fragmentSize, AbstractModelManager modelManager) {
		
		Vector<Fragment> fragmentVector = modelManager.createFragments(numFragments, fragmentSize);
		ArrayList<Set<String>>  summs = new ArrayList<Set<String>>(numFragments);
		ArrayList<Signature>  sigs200 = new ArrayList<Signature>(numFragments);
		ArrayList<Signature>  sigs100 = new ArrayList<Signature>(numFragments);
		ArrayList<Signature>  sigs50 = new ArrayList<Signature>(numFragments);
		ArrayList<Signature>  sigs20 = new ArrayList<Signature>(numFragments);
		ArrayList<Signature>  sigs10 = new ArrayList<Signature>(numFragments);
		ArrayList<Signature>  sigs5 = new ArrayList<Signature>(numFragments);
		for (int i = 0; i < numFragments; i++) {
			
			Fragment currentFragment = fragmentVector.get(i);
			Set<String> fSummary = modelManager.createFragmentSummary(currentFragment.getObjects());
			summs.add(fSummary);
			
			MinHash minh = new MinHash(200);
			Signature sig = new Signature(minh.minHashSig(fSummary));
			sigs200.add(sig);
			
			minh = new MinHash(100);
			sig = new Signature(minh.minHashSig(fSummary));
			sigs100.add(sig);
			
			minh = new MinHash(50);
			sig = new Signature(minh.minHashSig(fSummary));
			sigs50.add(sig);
			
			minh = new MinHash(20);
			sig = new Signature(minh.minHashSig(fSummary));
			sigs20.add(sig);
			
			minh = new MinHash(10);
			sig = new Signature(minh.minHashSig(fSummary));
			sigs10.add(sig);
			
			minh = new MinHash(5);
			sig = new Signature(minh.minHashSig(fSummary));
			sigs5.add(sig);
			
			
		}
		int fragNum = 0;
		System.out.println("fragNum, Jacc, s200, s100, s50, s20, s10, s5");
		for (int i = 0; i < numFragments; i++) {
			for (int j = 0; j < numFragments; j++) {
				if (i != j) {
					double simSig200 = HashComparator.hammingSim(sigs200.get(i).getSignature(), sigs200.get(j).getSignature());
					double simSig100 = HashComparator.hammingSim(sigs100.get(i).getSignature(), sigs100.get(j).getSignature());
					double simSig50 = HashComparator.hammingSim(sigs50.get(i).getSignature(), sigs50.get(j).getSignature());
					double simSig20 = HashComparator.hammingSim(sigs20.get(i).getSignature(), sigs20.get(j).getSignature());
					double simSig10 = HashComparator.hammingSim(sigs10.get(i).getSignature(), sigs10.get(j).getSignature());
					double simSig5 = HashComparator.hammingSim(sigs5.get(i).getSignature(), sigs5.get(j).getSignature());
					
					double simSumm = jaccard(summs.get(i), summs.get(j)) * 100;
					System.out.println(fragNum + ", " + Double.valueOf(df.format(simSumm)) + ", " 
					+ Double.valueOf(df.format(simSig200)) + ", "
					+ Double.valueOf(df.format(simSig100)) + ", " 
					+ Double.valueOf(df.format(simSig50)) + ", " 
					+ Double.valueOf(df.format(simSig20)) + ", " 
					+ Double.valueOf(df.format(simSig10)) + ", " 
					+ Double.valueOf(df.format(simSig5)));
					fragNum++;
				}
			}
		}
	}
	
	public static double jaccard(Set<String> a, Set<String> b) {
		final int sa = a.size();
	    final int sb = b.size();
	    Set<String> temp = new HashSet<String>();
	    temp.addAll(a);
	    temp.retainAll(b);
	    final int intersection = temp.size();
	    if (intersection != 0) {
	    	return 1d / (sa + sb - intersection) * intersection;
	    }else {
	    	return 0.0;
	    }
	}
	
	public int[] flatten(int[][] arr) {
		   return Stream.of(arr)
		      .flatMapToInt(row -> IntStream.of(row))
		      .toArray();
	}

}
