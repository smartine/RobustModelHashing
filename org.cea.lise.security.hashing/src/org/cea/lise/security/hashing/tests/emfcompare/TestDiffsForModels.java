package org.cea.lise.security.hashing.tests.emfcompare;

public class TestDiffsForModels {
	
	static String metamodelPath = "/home/smartinez/Work/Eclipses/eclipse/workspaces/wRobustHashing/Tests/tests/PetriNet.ecore";
	static String path = "/home/smartinez/Work/Eclipses/eclipse/workspaces/wRobustHashing/fr.inria.atlanmod.instantiator/PetriNet/PetriNet5";
	
	//static String metamodelPath = "/home/smartinez/Work/GitRepos/RobustModelHashing/org.cea.lise.security.hashing/TransModels/ATL.ecore";
	//static String path = "/home/smartinez/Work/GitRepos/mde-watermarking/org.cea.lise.security.watermarking.mutation/TrafoRepo";
	
	public static void main(String[] args) {
	
		GenericDiffsTest testEMF = new GenericDiffsTest();
		double [][] differences = testEMF.run(metamodelPath, path);
		testEMF.printV(differences);
	}
	
}
