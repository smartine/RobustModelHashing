package org.cea.lise.security.hashing.tests;

import java.io.File;
import java.util.Arrays;

import org.cea.lise.security.hashing.AttributeFilter;
import org.cea.lise.security.hashing.EMFModelManager;
import org.cea.lise.security.hashing.ModelHashing;
import org.cea.lise.security.hashing.ModelUtil;
import org.eclipse.emf.ecore.EObject;

public class TestEMFModelHash {
	static int minhashk = 20;
	static int numFragments = 30;
	static int numBuckets = 50;
	
	// First of all, we have to open the model, then, create N partitions, then, call minhash on it and finally, quantize it
	public static void main(String[] args) {
		
		String metamodelPath = "TransModels" + File.separator + "ATL.ecore";
		ModelUtil.registerMetamodel(metamodelPath);
		
		EObject root = ModelUtil.doLoadXMIFromPath("TransModels" + File.separator + "Trafo (1).xmi");
		
		
		AttributeFilter af = new AttributeFilter();
		af.add("location");
		af.add("commentsBefore");
		af.add("commentsAfter");
		
		EMFModelManager emfmodelmanager = new EMFModelManager(root, af);
		//EcoreMetamodelManager modelManager = new EcoreMetamodelManager(root);
		ModelHashing mh = new ModelHashing(100, 10, 5, 10, 80, 6, emfmodelmanager);
		long tStart = System.currentTimeMillis();
		int [] modelHash = mh.createModelHash(root);
		long tEnd = System.currentTimeMillis();
		long tDelta = tEnd - tStart;
		double elapsedSeconds = tDelta / 1000.0;
		System.out.println("Time: " + elapsedSeconds);
		System.out.println(Arrays.toString(modelHash));
	}

}
