package org.cea.lise.security.hashing;

import java.util.ArrayList;
import java.util.List;

public class BucketItem {
	List<Signature> itemList;

	public BucketItem(List<Signature> itemList) {
		super();
		this.itemList = itemList;
	}

	public List<Signature> getItemList() {
		return itemList;
	}

	public void setItemList(List<Signature> itemList) {
		this.itemList = itemList;
	}
	
	
	
}
