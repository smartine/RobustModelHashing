package org.cea.lise.security.hashing;

import java.util.Arrays;

public class Signature {
	
	int [] signature;

	public Signature(int[] signature) {
		super();
		this.signature = signature;
	}

	public int[] getSignature() {
		return signature;
	}

	public void setSignature(int[] signature) {
		this.signature = signature;
	}
	
	public int [] getBand(int from, int to) {
		return Arrays.copyOfRange(signature, from, to);
	}
	
	

}
